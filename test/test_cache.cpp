/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2015-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE Test_Cache
#include <boost/test/unit_test.hpp>
#include <boost/core/noncopyable.hpp>

#include "melanolib/data_structures/cache.hpp"

using Map = melanolib::LruCache<int, std::string>;

BOOST_AUTO_TEST_CASE( test_at_access )
{
    Map map(3);
    BOOST_CHECK_THROW(map.at(1), std::out_of_range);
    map.insert(1, "one");
    BOOST_CHECK_EQUAL(map.at(1), "one");
    BOOST_CHECK_THROW(map.at(2), std::out_of_range);
}

BOOST_AUTO_TEST_CASE( test_contains )
{
    Map map(3);
    BOOST_CHECK(!map.contains(1));
    map.insert(1, "one");
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK(!map.contains(2));
}

BOOST_AUTO_TEST_CASE( test_empty )
{
    Map map(3);
    BOOST_CHECK(map.empty());
    map.insert(1, "one");
    BOOST_CHECK(!map.empty());
}

BOOST_AUTO_TEST_CASE( test_size )
{
    Map map(3);
    BOOST_CHECK_EQUAL(map.size(), 0);
    map.insert(1, "one");
    BOOST_CHECK_EQUAL(map.size(), 1);
    map.insert(2, "two");
    BOOST_CHECK_EQUAL(map.size(), 2);
    map.insert(3, "three");
    BOOST_CHECK_EQUAL(map.size(), 3);
}

BOOST_AUTO_TEST_CASE( test_insert_overflow )
{
    Map map(3);
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");
    BOOST_CHECK(map.contains(1));
    map.insert(4, "four");
    BOOST_CHECK(!map.contains(1));
    BOOST_CHECK(map.contains(4));
    BOOST_CHECK_EQUAL(map.size(), 3);
}

BOOST_AUTO_TEST_CASE( test_insert_existing )
{
    Map map(3);
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");
    BOOST_CHECK(map.contains(1));
    map.insert(3, "III");
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK_EQUAL(map.size(), 3);
    BOOST_CHECK_EQUAL(map.at(3), "III");
}

BOOST_AUTO_TEST_CASE( test_resize )
{
    Map map(5);
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");
    map.insert(4, "four");
    map.insert(5, "five");
    map.set_max_size(3);
    BOOST_CHECK_EQUAL(map.size(), 3);
    BOOST_CHECK(!map.contains(1));
    BOOST_CHECK(!map.contains(2));
    BOOST_CHECK(map.contains(3));
    BOOST_CHECK(map.contains(4));
    BOOST_CHECK(map.contains(5));
}

BOOST_AUTO_TEST_CASE( test_at_reorder )
{
    Map map(5);
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");
    map.insert(4, "four");
    map.insert(5, "five");
    map.at(1);
    map.at(2);
    map.set_max_size(3);
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK(map.contains(2));
    BOOST_CHECK(!map.contains(3));
    BOOST_CHECK(!map.contains(4));
    BOOST_CHECK(map.contains(5));
}

BOOST_AUTO_TEST_CASE( test_insert_existing_reorder )
{
    Map map(5);
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");
    map.insert(1, "I");
    map.insert(4, "four");
    map.insert(5, "five");
    map.insert(2, "II");
    map.set_max_size(3);
    BOOST_CHECK(!map.contains(1));
    BOOST_CHECK(map.contains(2));
    BOOST_CHECK(!map.contains(3));
    BOOST_CHECK(map.contains(4));
    BOOST_CHECK(map.contains(5));
}

class MoveMe : private boost::noncopyable
{
public:
    MoveMe(){}
    MoveMe(MoveMe&&) {}
    MoveMe& operator=(MoveMe&&) { return *this; }
};

BOOST_AUTO_TEST_CASE( test_move_only )
{
    melanolib::LruCache<int, MoveMe> map(4);
    map.insert(1, MoveMe());
    map.insert(2, MoveMe());
    map.insert(3, MoveMe());
    map.insert(1, MoveMe());
    map.insert(4, MoveMe());
    map.insert(5, MoveMe());
    map.set_max_size(3);
    map.at(5);
}

using PtrMap = melanolib::SharedObjectLruCache<int, std::string>;

BOOST_AUTO_TEST_CASE( test_shared_no_linger )
{
    PtrMap map(5);
    map.insert(1, std::make_shared<std::string>("one"));
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(1, std::make_shared<std::string>("I"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    map.insert(2, std::make_shared<std::string>("II"));
    map.set_max_size(3);
    BOOST_CHECK(!map.contains(1));
    BOOST_CHECK(map.contains(2));
    BOOST_CHECK(!map.contains(3));
    BOOST_CHECK(map.contains(4));
    BOOST_CHECK(map.contains(5));
}

BOOST_AUTO_TEST_CASE( test_shared_contains_check_linger )
{
    PtrMap map(5);
    auto one = std::make_shared<std::string>("one");
    map.insert(1, one);
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    map.set_max_size(3);
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK(!map.contains(2));
    BOOST_CHECK(!map.contains(3));
    BOOST_CHECK(map.contains(4));
    BOOST_CHECK(map.contains(5));
    map.insert(6, std::make_shared<std::string>("6"));
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK(!map.contains(2));
    BOOST_CHECK(!map.contains(3));
    BOOST_CHECK(!map.contains(4));
    BOOST_CHECK(map.contains(5));
    BOOST_CHECK(map.contains(6));
    one.reset();
    map.set_max_size(1);;
    BOOST_CHECK(!map.contains(1));
    BOOST_CHECK(map.contains(6));
}

BOOST_AUTO_TEST_CASE( test_shared_expired_linger )
{
    PtrMap map(3);
    auto one = std::make_shared<std::string>("one");
    auto two = std::make_shared<std::string>("two");
    auto three = std::make_shared<std::string>("three");
    auto four = std::make_shared<std::string>("four");
    auto five = std::make_shared<std::string>("five");
    map.insert(1, one);
    map.insert(2, two);
    map.insert(3, three);
    map.insert(4, four);
    map.insert(5, five);
    BOOST_CHECK(!map.contains(1, false));
    BOOST_CHECK(!map.contains(2, false));
    BOOST_CHECK(map.contains(3, false));
    BOOST_CHECK(map.contains(4, false));
    BOOST_CHECK(map.contains(5, false));

    two.reset();
    BOOST_CHECK(map.contains(1));
    BOOST_CHECK(!map.contains(2, false));
    BOOST_CHECK(!map.contains(3, false));

    BOOST_CHECK(!map.contains(2));

    three.reset();
    BOOST_CHECK(!map.contains(3));
}

BOOST_AUTO_TEST_CASE( test_shared_contains_no_check_linger )
{
    PtrMap map(5);
    auto one =  std::make_shared<std::string>("one");
    map.insert(1, one);
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    map.set_max_size(3);
    BOOST_CHECK(!map.contains(1, false));
    BOOST_CHECK(!map.contains(2, false));
    BOOST_CHECK(map.contains(3, false));
    BOOST_CHECK(map.contains(4, false));
    BOOST_CHECK(map.contains(5, false));
    map.insert(6, std::make_shared<std::string>("6"));
    BOOST_CHECK(!map.contains(1, false));
    BOOST_CHECK(!map.contains(2, false));
    BOOST_CHECK(!map.contains(3, false));
    BOOST_CHECK(map.contains(4, false));
    BOOST_CHECK(map.contains(5, false));
    BOOST_CHECK(map.contains(6, false));
}

BOOST_AUTO_TEST_CASE( test_shared_clear )
{
    PtrMap map(5);
    auto one =  std::make_shared<std::string>("one");
    map.insert(1, one);
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    map.clear();
    BOOST_CHECK(!map.contains(1, false));
    BOOST_CHECK(map.contains(1));
}

BOOST_AUTO_TEST_CASE( test_shared_at )
{
    PtrMap map(3);
    auto one =  std::make_shared<std::string>("one");
    map.insert(1, one);
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    BOOST_CHECK_EQUAL(*map.at(1), "one");
    one.reset();
    BOOST_CHECK_EQUAL(*map.at(5), "five");
    BOOST_CHECK_THROW(map.at(2), std::out_of_range);
    BOOST_CHECK_THROW(map.at(3), std::out_of_range);
    map.at(4);
    map.insert(6, std::make_shared<std::string>("six"));
    BOOST_CHECK_THROW(map.at(1), std::out_of_range);
}

BOOST_AUTO_TEST_CASE( test_shared_size_shared )
{
    PtrMap map(3);
    auto one =  std::make_shared<std::string>("one");
    map.insert(1, one);
    map.insert(2, std::make_shared<std::string>("two"));
    map.insert(3, std::make_shared<std::string>("three"));
    map.insert(4, std::make_shared<std::string>("four"));
    map.insert(5, std::make_shared<std::string>("five"));
    BOOST_CHECK_EQUAL(map.size(), 3);
    BOOST_CHECK_EQUAL(map.size_shared(), 4);
    one.reset();
    BOOST_CHECK_EQUAL(map.size_shared(), 3);
}
