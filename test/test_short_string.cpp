/**
 * \file
 * \author Mattia Basaglia
 * \copyright Copyright 2015-2017 Mattia Basaglia
 * \section License
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE Test_ShortString

#include <sstream>
#include <boost/test/unit_test.hpp>

#include "melanolib/string/short_string.hpp"

using namespace melanolib::string;

using String = short_string<8>;

template<std::size_t Capacity>
void check_string(const short_string<Capacity>& subj, const std::string& cmp)
{
    BOOST_CHECK_EQUAL(subj.size(), cmp.size());
    BOOST_CHECK_EQUAL(subj.data(), cmp);
}

BOOST_AUTO_TEST_CASE( test_ctor )
{
    check_string(String(), "");
    check_string(String("foobar"), "foobar");
    String lval("foobar");
    check_string(String(lval), "foobar");
    check_string(String(std::move(lval)), "foobar");
    check_string(String("foobar", 3), "foo");
    check_string(String({'f', 'o', 'o'}), "foo");
    check_string(String(short_string<6>("foobar")), "foobar");
    check_string(String(short_string<10>("helloworld"), 5, 5), "world");
    std::string str = "foobar";
    check_string(String(str.begin(), str.end()), str);
}

BOOST_AUTO_TEST_CASE( test_make_short_string )
{
    auto str = make_short_string<char, 'f', 'o', 'o', 'b', 'a', 'r', '!', '!'>();
    check_string(str, "foobar!!");
    BOOST_CHECK_EQUAL(str.capacity(), 8);
}

BOOST_AUTO_TEST_CASE( test_iterators )
{
    String str = "foobar";
    const String& cstr = str;
    String empty;

    BOOST_CHECK(empty.begin() == empty.end());
    BOOST_CHECK(str.begin() < str.end());
    BOOST_CHECK_EQUAL(str.begin(), str.cbegin());
    BOOST_CHECK_EQUAL(str.begin(), cstr.begin());
    BOOST_CHECK_EQUAL(*str.begin(), 'f');
    BOOST_CHECK_EQUAL(*(str.begin()+1), 'o');
    BOOST_CHECK_EQUAL((void*)str.end(), (void*)(str.begin() + str.size()));
    BOOST_CHECK_EQUAL((void*)str.end(), (void*)str.cend());
    BOOST_CHECK_EQUAL((void*)str.end(), (void*)cstr.end());
    BOOST_CHECK_EQUAL(*(str.end()-1), 'r');
}


BOOST_AUTO_TEST_CASE( test_reverse_iterators )
{
    String str = "foobar";
    const String& cstr = str;
    String empty;

    BOOST_CHECK(empty.rbegin() == empty.rend());
    BOOST_CHECK(str.rbegin() < str.rend());
    BOOST_CHECK(str.rbegin() == str.crbegin());
    BOOST_CHECK(str.rbegin() == cstr.rbegin());
    BOOST_CHECK_EQUAL(*str.rbegin(), 'r');
    BOOST_CHECK_EQUAL(*(str.rbegin()+1), 'a');
    BOOST_CHECK(str.rend() == str.rbegin() + str.size());
    BOOST_CHECK(str.rend() == str.crend());
    BOOST_CHECK(str.rend() == cstr.rend());
    BOOST_CHECK_EQUAL(*(str.rend()-1), 'f');
}

BOOST_AUTO_TEST_CASE( test_capacity )
{
    BOOST_CHECK_EQUAL(String("foobar").size(), 6);
    BOOST_CHECK_EQUAL(String("foobar").length(), 6);
    BOOST_CHECK_EQUAL(String("foobar").capacity(), 8);
    BOOST_CHECK_EQUAL(String("foobar").max_size(), 8);
    BOOST_CHECK(!String("foobar").empty());

    BOOST_CHECK_EQUAL(String().size(), 0);
    BOOST_CHECK_EQUAL(String().capacity(), 8);
    BOOST_CHECK(String().empty());
}

BOOST_AUTO_TEST_CASE( test_resize )
{
    String str = "foobar";
    str.resize(3);
    check_string(str, "foo");
    str.resize(5, '!');
    check_string(str, "foo!!");
    str.resize(10, '?');
    check_string(str, "foo!!???");
    str.clear();
    BOOST_CHECK(str.empty());
}

BOOST_AUTO_TEST_CASE( test_element_access )
{
    String str = "foobar";
    BOOST_CHECK_EQUAL(str.front(), 'f');
    BOOST_CHECK_EQUAL(str[1], 'o');
    BOOST_CHECK_EQUAL(str.at(3), 'b');
    BOOST_CHECK_EQUAL(str.back(), 'r');
    BOOST_CHECK_THROW(str.at(7), std::out_of_range);
}

BOOST_AUTO_TEST_CASE( test_element_access_const )
{
    const String str = "foobar";
    BOOST_CHECK_EQUAL(str.front(), 'f');
    BOOST_CHECK_EQUAL(str[1], 'o');
    BOOST_CHECK_EQUAL(str.at(3), 'b');
    BOOST_CHECK_EQUAL(str.back(), 'r');
    BOOST_CHECK_THROW(str.at(7), std::out_of_range);
}

BOOST_AUTO_TEST_CASE( test_data_access )
{
    String str = "foobar";
    BOOST_CHECK_EQUAL(str.data(), str.c_str());
    BOOST_CHECK_EQUAL(str.data()[3], 'b');
    BOOST_CHECK_EQUAL(str.data()[str.size()], '\0');
}

BOOST_AUTO_TEST_CASE( test_data_access_const )
{
    const String str = "foobar";
    BOOST_CHECK_EQUAL(str.data(), str.c_str());
    BOOST_CHECK_EQUAL(str.data()[3], 'b');
    BOOST_CHECK_EQUAL(str.data()[str.size()], '\0');
}

BOOST_AUTO_TEST_CASE( test_push_pop_back )
{
    String str = "foobar";
    str.push_back('!');
    check_string(str, "foobar!");
    str.push_back('!');
    check_string(str, "foobar!!");
    str.push_back('!');
    check_string(str, "foobar!!");
    str.pop_back();
    str.pop_back();
    check_string(str, "foobar");
    str.pop_back();
    str.pop_back();
    str.pop_back();
    str.pop_back();
    str.pop_back();
    check_string(str, "f");
    str.pop_back();
    str.pop_back();
    str.pop_back();
    str.pop_back();
    check_string(str, "");
}

BOOST_AUTO_TEST_CASE( test_copy )
{
    char out[3] = {};
    String str = "foobar";
    BOOST_CHECK_EQUAL(str.copy(out, 2, 3), 2);
    BOOST_CHECK_EQUAL(std::string(out), std::string("ba"));
    char bigbuf[12] = {};
    BOOST_CHECK_EQUAL(str.copy(bigbuf, 12, 3), 3);
    BOOST_CHECK_EQUAL(bigbuf, std::string("bar"));

}

BOOST_AUTO_TEST_CASE( test_swap )
{
    String str = "foobar";
    String str2 = "hello";
    str.swap(str2);
    check_string(str, "hello");
    check_string(str2, "foobar");
}

BOOST_AUTO_TEST_CASE( test_compare )
{
    BOOST_CHECK(String("foobar").compare("foobar") == 0);
    BOOST_CHECK(String("foobar").compare("foobaz") < 0);
    BOOST_CHECK(String("foobaz").compare("foobar") > 0);
    BOOST_CHECK(String("foo").compare("foobar") < 0);
    BOOST_CHECK(String("foobar").compare("foo") > 0);
}

BOOST_AUTO_TEST_CASE( test_plus )
{
    auto cat = String("foo") + String("bar");
    check_string(cat, "foobar");
    BOOST_CHECK_EQUAL(cat.capacity(), 16);
}

BOOST_AUTO_TEST_CASE( test_cmp_ops )
{
    BOOST_CHECK(  String("foobar") == String("foobar") );
    BOOST_CHECK(!(String("foobar") == String("foobaz")));
    BOOST_CHECK(!(String("foobaz") == String("foobar")));


    BOOST_CHECK(!(String("foobar") != String("foobar")));
    BOOST_CHECK(  String("foobar") != String("foobaz") );
    BOOST_CHECK(  String("foobaz") != String("foobar") );

    BOOST_CHECK(!(String("foobar") < String("foobar")));
    BOOST_CHECK( (String("foobar") < String("foobaz")));
    BOOST_CHECK(!(String("foobaz") < String("foobar")));

    BOOST_CHECK(!(String("foobar") > String("foobar")));
    BOOST_CHECK(!(String("foobar") > String("foobaz")));
    BOOST_CHECK( (String("foobaz") > String("foobar")));

    BOOST_CHECK( (String("foobar") <= String("foobar")));
    BOOST_CHECK( (String("foobar") <= String("foobaz")));
    BOOST_CHECK(!(String("foobaz") <= String("foobar")));

    BOOST_CHECK( (String("foobar") >= String("foobar")));
    BOOST_CHECK(!(String("foobar") >= String("foobaz")));
    BOOST_CHECK( (String("foobaz") >= String("foobar")));
}

BOOST_AUTO_TEST_CASE( test_to_string )
{
    BOOST_CHECK_EQUAL( String("foobar").to_string(), std::string("foobar") );
}

BOOST_AUTO_TEST_CASE( test_io )
{
    String str("foobar");
    std::stringstream ss;
    ss << str;
    BOOST_CHECK_EQUAL(ss.str(), "foobar");
    str.clear();
    ss.seekg(0, std::ios::beg);
    ss >> str;
    check_string(str, "foobar");
}
