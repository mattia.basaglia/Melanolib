/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2015-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOLIB_RANGE_HPP
#define MELANOLIB_RANGE_HPP

#include <utility>
#include <vector>
#include <functional>
#include <algorithm>

namespace melanolib {

/**
 * \brief Simple wrapper around a pair of iterators that expresses a range
 */
template<class Iter>
class iterator_range
{
public:
    using iterator = Iter;

    iterator_range(iterator first, iterator second)
        : _begin(std::move(first)),
          _end(std::move(second))
    {}

    iterator begin() const { return _begin; }
    iterator end() const { return _end; }

private:
    iterator _begin;
    iterator _end;
};

/**
 * \brief Creates an iterator range out of a pair
 */
template<class Iter>
    auto make_range(const std::pair<Iter, Iter>& pair)
    {
        return iterator_range(pair.first, pair.second);
    }

/**
 * \brief Returns a sorted view of the given range,
 *
 * The range is sorted based on the initial values of its elements.
 *
 * \tparam FwIter   Forward iterator type
 * \tparam Compare  A strict ordering relation
 *
 * \todo Test this
 */
template<class FwIter, class Compare=std::less<FwIter>>
class SortedRange
{
public:
    using value_type = typename FwIter::value_type;
    using pointer = typename FwIter::pointer;
    using reference = typename FwIter::reference;

    class iterator : public std::vector<FwIter>::const_iterator
    {
    private:
        using Parent = std::vector<FwIter>::const_iterator;
        using MidRef = Parent::reference;

    public:
        using Parent::Parent;

        reference operator*() const
        {
            return *mid_ref();
        }

        pointer operator->() const
        {
            return mid_ref().operator->();
        }

    private:
        MidRef mid_ref() const
        {
            return Parent::operator*();
        }
    };

    SortedRange(FwIter begin, FwIter end, Compare cmp = {})
    {
        while ( begin != end )
        {
            iterators.push_back(begin++);
        }

        std::sort(iterators.begin(), iterators.end(),
            [&cmp](const FwIter& a, const FwIter& b) {
                return cmp(*a, *b);
        });
    }

    iterator begin() const
    {
        return iterator(iterators.begin());
    }

    iterator end() const
    {
        return iterator(iterators.end());
    }

private:
    std::vector<FwIter> iterators;
};

} // namespace melanolib
#endif // MELANOLIB_RANGE_HPP
