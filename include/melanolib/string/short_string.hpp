/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2015-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOLIB_STRING_SHORT_STRING_HPP
#define MELANOLIB_STRING_SHORT_STRING_HPP

#include <string>
#include <stdexcept>
#include <limits>
#include <iostream>
#include <utility>

#include <melanolib/math/math.hpp>

namespace melanolib {
namespace string {

/**
 * \brief String class with a fixed maximum size that can be used at compile time
 * \note no char traits because they are non-constexpr
 */
template<std::size_t Capacity, class Char = char>
class short_string
{
private:
    template<std::size_t Size>
        using EnableSmaller = std::enable_if_t<(Size < Capacity), int>;

    template<std::size_t Size>
        using Resized = short_string<Size, Char>;

    static_assert(Capacity <= std::numeric_limits<Char>::max(), "Capacity is too large");

public:
// Types
    using value_type             = Char;
    using pointer                = value_type*;
    using const_pointer          = const value_type*;
    using reference              = value_type&;
    using const_reference        = const value_type&;
    using iterator               = value_type*;
    using const_iterator         = const value_type*;
    using size_type              = decltype(Capacity);
    using difference_type        = std::ptrdiff_t;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    static constexpr size_type npos = size_type(-1);

// Construction and Assignment
    /**
     * \brief Constructs an empty string
     */
    constexpr short_string() noexcept
    {
        set_size(0);
    }

    constexpr short_string(const short_string&) noexcept = default;
    constexpr short_string(short_string&&) noexcept = default;

    constexpr short_string& operator=(const short_string&) noexcept = default;
    constexpr short_string& operator=(short_string&&) noexcept = default;

    /**
     * \brief Copies characters from the string,
     * terminating after the smallest of str.size() - pos, capacity() or count
     */
    template<size_type Size>
    constexpr short_string(const Resized<Size>& str, size_type pos,
                           size_type count = Size) noexcept
    {
        assign(str, pos, count);
    }

    /**
     * \brief Creates a string with \p count (or at most capacity()) copies of \p ch
     */
    constexpr short_string(size_type count, value_type ch) noexcept
    {
        assign(count, ch);
    }

    /**
     * \brief Copies at most capacity() characters from the string,
     * terminating at the first NUL characher or when it would exceed the
     * allowed capacity
     */
    constexpr short_string(const char* cstr) noexcept
    {
        assign(cstr);
    }

    /**
     * \brief Copies characters from the string,
     * terminating after the smallest of \p length and capacity()
     */
    constexpr short_string(const char* cstr, size_type length) noexcept
    {
        assign(cstr, length);
    }

    /**
     * \brief Copies (at most capacity()) characters from an initializer list
     */
    constexpr short_string(const std::initializer_list<value_type>& il) noexcept
    {
        assign(il);;
    }

    /**
     * \brief Copies characters from a smaller short_string
     */
    template<size_type Smaller, EnableSmaller<Smaller> = 0>
        constexpr short_string(const Resized<Smaller>& oth) noexcept
        {
            assign(oth);
        }

    /**
     * \brief Copies characters from the input range, terminating after
     * the smallest of std::distance(begin, end) and capacity()
     */
    template<class InputIterator>
    constexpr short_string(InputIterator first, InputIterator last)
    {
        assign(first, last);
    }

// Iterator Support
    constexpr const_iterator begin() const noexcept { return _storage; }
    constexpr const_iterator cbegin() const noexcept { return _storage; }
    constexpr iterator begin() noexcept { return _storage; }

    constexpr const_iterator end() const noexcept { return _storage + size(); }
    constexpr const_iterator cend() const noexcept { return _storage + size(); }
    constexpr iterator end() noexcept { return _storage + size(); }

    constexpr const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    constexpr const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(end()); }
    constexpr reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    constexpr const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
    constexpr const_reverse_iterator crend() const noexcept { return const_reverse_iterator(begin()); }
    constexpr reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

// Capacity
    /**
     * \brief Number of characters actually used by the string
     * (Always <= capacity())
     */
    constexpr size_type size() const noexcept
    {
        return Capacity - _storage[Capacity];
    }

    /**
     * \brief Alias for size()
     */
    constexpr size_type length() const noexcept
    {
        return size();
    }

    /**
     * \brief Maximum number of characters that can be stored
     */
    constexpr size_type capacity() const noexcept
    {
        return Capacity;
    }

    /**
     * \brief Alias for capacity()
     */
    constexpr size_type max_size() const noexcept
    {
        return Capacity;
    }

    /**
     * \brief Whether size() == 0
     */
    constexpr bool empty() const noexcept
    {
        return size() == 0;
    }

    /**
     * \brief Changes the size of the string (up to capacity())
     *
     * Overwrites existing characters with \p ch when expanding
     */
    constexpr void resize(size_type size, value_type ch) noexcept
    {
        if ( size > Capacity )
            size = Capacity;
        if ( size > this->size() )
            fill_n(size - this->size(), ch, _storage + this->size());
        set_size(size);
    }

    /**
     * \brief Changes the size of the string (up to capacity())
     *
     * Leaves existing characters unchanged even when expanding
     */
    constexpr void resize(size_type size) noexcept
    {
        set_size(math::min(size, Capacity));
    }


    /**
     * \post size() == 0
     */
    constexpr void clear() noexcept
    {
        set_size(0);
    }

// Element access
    /**
     * \brief Element access
     * \pre \p pos < size()
     */
    constexpr value_type operator[](size_type pos) const noexcept
    {
        return _storage[pos];
    }

    constexpr reference operator[](size_type pos) noexcept
    {
        return _storage[pos];
    }

    /**
     * \brief Element access
     * \throws std::out_of_range if pos >= size()
     */
    constexpr value_type at(size_type pos) const
    {
        if ( pos >= size() )
            throw std::out_of_range("Element out of range");
        return _storage[pos];
    }

    constexpr reference at(size_type pos)
    {
        if ( pos >= size() )
            throw std::out_of_range("Element out of range");
        return _storage[pos];
    }

    /**
     * \brief Returns the first element
     * \pre !empty()
     */
    constexpr value_type front() const noexcept
    {
        return _storage[0];
    }

    constexpr reference front() noexcept
    {
        return _storage[0];
    }

    /**
     * \brief Returns the last element
     * \pre !empty()
     */
    constexpr value_type back() const noexcept
    {
        return _storage[size() - 1];
    }

    constexpr reference back() noexcept
    {
        return _storage[size() - 1];
    }

    /**
     * \brief Exposes the underlying data, data()[size()] will always be NUL
     */
    constexpr pointer data() noexcept
    {
        return _storage;
    }

    constexpr const_pointer data() const noexcept
    {
        return _storage;
    }

    /**
     * \brief Alias for data()
     */
    constexpr pointer c_str() noexcept
    {
        return _storage;
    }

    constexpr const_pointer c_str() const noexcept
    {
        return _storage;
    }

// Modifiers
    constexpr short_string& assign(const short_string& oth) noexcept
    {
        return *this = oth;
    }

    constexpr short_string& assign(short_string&& oth) noexcept
    {
        return *this = std::move(oth);
    }

    /**
     * \brief Copies characters from the string,
     * terminating after the smallest of str.size() - pos, capacity() or count
     */
    template<size_type Size>
    constexpr short_string& assign(const Resized<Size>& str, size_type pos,
                                   size_type count = Size) noexcept
    {
        size_type i = 0;
        for ( ; i < count && i <= Capacity && pos + i < str.size(); i++ )
            _storage[i] = str[pos + i];
        set_size(i);
        return *this;
    }

    /**
     * \brief Creates a string with \p count (or at most capacity()) copies of \p ch
     */
    constexpr short_string& assign(size_type count, value_type ch) noexcept
    {
        if ( count > Capacity )
            count = Capacity;
        fill_n(count, ch, _storage);
        set_size(count);
        return *this;
    }

    /**
     * \brief Copies at most capacity() characters from the string,
     * terminating at the first NUL characher or when it would exceed the
     * allowed capacity
     */
    constexpr short_string& assign(const char* cstr) noexcept
    {
        size_type i = 0;
        while ( *cstr && i <= Capacity )
            _storage[i++] = *cstr++;
        set_size(i);
        return *this;
    }

    /**
     * \brief Copies characters from the string,
     * terminating after the smallest of \p length and capacity()
     */
    constexpr short_string& assign(const char* cstr, size_type length) noexcept
    {
        size_type i = 0;
        while ( i <= Capacity && i < length )
            _storage[i++] = *cstr++;
        set_size(i);
        return *this;
    }

    /**
     * \brief Copies (at most capacity()) characters from an initializer list
     */
    constexpr short_string& assign(const std::initializer_list<value_type>& il) noexcept
    {
        size_type i = 0;
        for ( value_type c : il )
        {
            if ( i >= Capacity )
                break;
            _storage[i++] = c;
        }
        set_size(i);
        return *this;
    }

    /**
     * \brief Copies characters from a smaller short_string
     */
    template<size_type Smaller, EnableSmaller<Smaller> = 0>
        constexpr short_string& assign(const Resized<Smaller>& oth) noexcept
        {
            copy_n(Smaller, oth.data(), _storage);
            set_size(oth.size());
            return *this;
        }

    /**
     * \brief Copies characters from the input range, terminating after
     * the smallest of std::distance(begin, end) and capacity()
     */
    template<class InputIterator>
    constexpr short_string& assign(InputIterator first, InputIterator last)
    {
        size_type i = 0;
        while ( first != last && i < Capacity )
            _storage[i++] = *first++;
        set_size(i);
        return *this;
    }

    /**
     * \brief Adds a character at the end of the string
     * \pre size() < Capacity
     */
    constexpr void push_back(value_type ch) noexcept
    {
        if ( size() < Capacity )
        {
            _storage[size()] = ch;
            set_size(size() + 1);
        }
    }

    /**
     * \brief Removes a character off the end of the string
     * \pre !empty()
     */
    constexpr void pop_back() noexcept
    {
        if ( !empty() )
        {
            set_size(size() - 1);
        }
    }

    /**
     * \pre \p pos < size() && \p n < size() - \p pos &&
     *      \p output points to a range of at least \p n characters
     */
    constexpr size_type copy(pointer output, size_type n, size_type pos = 0) noexcept
    {
        auto rlen = std::min(size() - pos, n);
        copy_n(rlen, _storage + pos, output);
        return rlen;
    }

    constexpr void swap(short_string& oth) noexcept
    {
        for ( size_type i = 0; i <= Capacity; i++ )
        {
            auto t = _storage[i];
            _storage[i] = oth._storage[i];
            oth._storage[i] = t;
        }
    }

// String Operations
    template<size_type Size, EnableSmaller<Size> = 0>
    constexpr Resized<Size> prefix() const noexcept
    {
        return Resized<Size>(*this, 0, Size);
    }

    template<size_type Size, EnableSmaller<Size> = 0>
    constexpr Resized<Size> suffix() const noexcept
    {
        return Resized<Size>(*this, size() - Size);
    }

    template<size_type Offset, size_type Size>
    constexpr Resized<Size> substr() const noexcept
    {
        return Resized<Size>(*this, Offset, Size);
    }

    int compare(const short_string& oth) const noexcept
    {
        for ( size_type i = 0; i < math::min(size(), oth.size()); i++ )
        {
            auto cmp = math::compare(_storage[i], oth._storage[i]);
            if ( cmp )
                return cmp;
        }
        return math::compare(size(), oth.size());
    }

    /**
     * \brief Adds two strings
     * \return A short_string large enough to contain all the characters
     */
    template<std::size_t OthCap>
    constexpr auto operator+(const short_string<OthCap, Char>& rhs) const noexcept
    {
        short_string<Capacity + OthCap, Char> ret = *this;
        copy_n(rhs.size(), rhs.data(), ret.data() + size());
        ret.set_size(size() + rhs.size());
        return ret;
    }

    constexpr bool operator==(const short_string& oth) const noexcept
    {
        return compare(oth) == 0;
    }

    constexpr bool operator!=(const short_string& oth) const noexcept
    {
        return !(*this == oth);
    }

    constexpr bool operator<(const short_string& oth) const noexcept
    {
        return compare(oth) < 0;
    }


    constexpr bool operator<=(const short_string& oth) const noexcept
    {
        return compare(oth) <= 0;
    }

    constexpr bool operator>(const short_string& oth) const noexcept
    {
        return compare(oth) > 0;
    }


    constexpr bool operator>=(const short_string& oth) const noexcept
    {
        return compare(oth) >= 0;
    }

// Conversion
    std::string to_string() const
    {
        return std::string(data(), size());
    }

private:
    /**
     * \brief Copies \p n characters from \p from to \p to (possibly at compile time)
     * \pre Both \p from and \p to point to sequences of at least \p n characters
     */
    static constexpr void copy_n(size_type n, const_pointer from, pointer to) noexcept
    {
        for ( size_type i = 0; i < n; i++ )
        {
            *to++ = *from++;
        }
    }

    /**
     * \brief Fills \p n copies of \p ch characters into \p to (possibly at compile time)
     * \pre \p to points to sequences of at least \p n characters
     */
    static constexpr void fill_n(size_type n, value_type ch, pointer to) noexcept
    {
        for ( size_type i = 0; i < n; i++ )
        {
            *to++ = ch;
        }
    }

    /**
     * \brief Sets the size of the string
     * \pre size <= Capacity
     * \post this->size() == size && (*this)[size] == NUL
     */
    constexpr void set_size(size_type size) noexcept
    {
        _storage[Capacity] = Capacity - size;
        _storage[size] = 0;
    }

    char _storage[Capacity + 1];

    template<std::size_t Capacity2, class Char2>
    friend class short_string;

    template<class Char2, Char2... chars>
    friend constexpr auto make_short_string() noexcept;
};

namespace detail
{
    template<class... Args>
    constexpr void eat_dots(Args...) noexcept
    {}

    template<class Char>
    constexpr Char assign_one(Char* out, std::pair<std::size_t, Char> p) noexcept
    {
        return out[p.first] = p.second;
    }

    template<class Char, Char... chars, std::size_t... indices>
    constexpr void assign(Char* out, std::index_sequence<indices...>) noexcept
    {
        eat_dots(assign_one(out, std::make_pair(indices, chars))...);
    }
}

/**
 * \brief Creates a short_string given from template parameters
 */
template<class Char, Char... chars>
constexpr auto make_short_string() noexcept
{
    constexpr auto capacity = sizeof...(chars);
    short_string<capacity, Char> str;
    detail::assign<Char, chars...>(str.data(), std::make_index_sequence<capacity>{});
    str.resize(capacity);
    return str;
}

template<std::size_t Capacity, class CharT, class Traits>
    std::basic_istream<CharT, Traits>& operator>>(
        std::basic_istream<CharT, Traits>& is,
        short_string<Capacity, CharT>& str)
{
    typename std::basic_istream<CharT, Traits>::sentry sentry(is);
    if ( sentry )
    {
        is.read(str.data(), str.max_size());
        str.resize(is.gcount());
        is.width(0);
    }
    return is;
}

template<std::size_t Capacity, class CharT, class Traits>
    std::basic_ostream<CharT, Traits>& operator<<(
        std::basic_ostream<CharT, Traits>& os,
        const short_string<Capacity, CharT>& str)
{
    return os << str.c_str();
}

} // namespace string
} // namespace melanolib
#endif // MELANOLIB_STRING_SHORT_STRING_HPP
