/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOLIB_MELANOLIB_DATA_STRUCTURES_RAW_BUFFER_HPP
#define MELANOLIB_MELANOLIB_DATA_STRUCTURES_RAW_BUFFER_HPP

#include <new>
#include <cstddef>

namespace melanolib{

/**
 * \brief RAII object around a chunk of memory
 */
class RawBuffer
{
public:
    using size_type = std::size_t;
    using pointer = void*;

    /**
     * \brief Allocates a chunk of data
     * \throws std::bad_alloc if the allocation failed
     */
    explicit RawBuffer(size_type size)
        : _data(operator new(size)), _size(size)
    {}

    /**
     * \brief Allocates a chunk of data
     *
     * If the allocation fails, it will be as if default constructed.
     */
    explicit RawBuffer(size_type size, std::nothrow_t) noexcept
        :  _data(operator new(size, std::nothrow)), _size(_data ? size : 0)
    {}

    /**
     * \brief Constructs an empty buffer
     */
    RawBuffer() noexcept
    {}

    /**
     * \brief Constructs an empty buffer
     */
    explicit RawBuffer(std::nullptr_t) noexcept
    {}

    RawBuffer(RawBuffer&& other) noexcept
        : _data(other._data), _size(other._size)
    {
        other._data = nullptr;
        other._size = 0;
    }

    RawBuffer(const RawBuffer&) = delete;

    RawBuffer& operator=(RawBuffer&& other) noexcept
    {
        swap(other);
        return *this;
    }

    RawBuffer& operator=(const RawBuffer&) = delete;

    /**
     * \brief Frees the allocated buffer
     */
    ~RawBuffer()
    {
        operator delete(_data, std::nothrow);
    }

    size_type size() const noexcept
    {
        return _size;
    }

    explicit operator bool() const noexcept
    {
        return _data;
    }

    /**
     * \brief Deallocates the buffer and sets this object to be null
     */
    void reset()
    {
        operator delete(_data);
        _data = nullptr;
        _size = 0;
    }

    void reset(std::nothrow_t) noexcept
    {
        operator delete(_data, std::nothrow);
        _data = nullptr;
        _size = 0;
    }

    /**
     * \brief Reallocates the buffer with one of the given size
     */
    void resize(std::size_t size)
    {
        if ( size > _size )
        {
            reset();
            _data = operator new(size);
        }

        _size = size;
    }

    /**
     * \brief Reallocates the buffer with one of the given size
     */
    void resize(std::size_t size, std::nothrow_t) noexcept
    {
        if ( size > _size )
        {
            reset();
            _data = operator new(size, std::nothrow);
        }

        _size = size;
    }

    /**
     * \brief Swaps the managed buffer
     */
    void swap(RawBuffer& other) noexcept
    {
        std::swap(other._data, _data);
        std::swap(other._size, _size);
    }

    /**
     * \brief Returns the managed buffer
     */
    pointer get() const noexcept
    {
        return _data;
    }

    /**
     * \brief Returns the managed buffer
     */
    pointer data() const noexcept
    {
        return _data;
    }

private:
    pointer _data = nullptr;
    size_type _size = 0;
};

} // namespace melanolib
#endif // MELANOLIB_MELANOLIB_DATA_STRUCTURES_RAW_BUFFER_HPP
