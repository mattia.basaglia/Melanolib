/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOLIB_MELANOLIB_DATA_STRUCTURES_CACHE_HPP
#define MELANOLIB_MELANOLIB_DATA_STRUCTURES_CACHE_HPP

#include <unordered_map>
#include <list>
#include <iterator>
#include <memory>
#include <algorithm>

#include "melanolib/data_structures/hash.hpp"

namespace melanolib{

namespace detail {

/**
 * \brief Least-recently used cache
 */
template <class Key, class T, class Hash>
class LruCacheCommon
{
public:
    using key_type = Key;
    using mapped_type = T;
    using size_type = std::size_t;
    using hasher = Hash;

    explicit LruCacheCommon(size_type max_size)
        : max(max_size)
    {}

    /**
     * \brief Number of items in the cache
     *
     * Complexity: constant
     */
    size_type size() const
    {
        return map.size();
    }

    /**
     * \brief Number of allowed items
     *
     * Complexity: constant
     */
    size_type max_size() const
    {
        return max;
    }

    /**
     * \brief Whether the cache has no elements
     *
     * Complexity: constant
     */
    bool empty() const
    {
        return map.empty();
    }

protected:
    struct Node;
    using Map = std::unordered_map<key_type, Node, Hash>;
    using Usage = std::list<key_type>;

    struct Node
    {
        typename Usage::iterator pos;
        mapped_type value;
    };

    /**
     * \brief Updates the usage list so that \p node appears on front()
     */
    void touch(Node& node)
    {
        // Move node.pos to the front of the list without ivalidating
        // any iterators
        usage.splice(usage.begin(), usage, node.pos);
    }

    void pop_back()
    {
        // Erase the last-used node
        map.erase(usage.back());
        // Remove it from usage
        usage.pop_back();
    }

    Map map;
    Usage usage;
    size_type max;

};

} // namespace detail

/**
 * \brief Least-recently used cache
 */
template <class Key, class T, class Hash = Hasher<Key>>
class LruCache : public detail::LruCacheCommon<Key, T, Hash>
{
private:
    using Parent = detail::LruCacheCommon<Key, T, Hash>;
    using Node = typename Parent::Node;

public:
    using key_type = typename Parent::key_type;
    using mapped_type = typename Parent::mapped_type;
    using size_type = typename Parent::size_type;

    explicit LruCache(size_type max_size)
        : Parent(max_size)
    {}

    /**
     * \brief Update the number of allowed items
     *
     * Complexity: constant if \p max_size >= size()
     * otherwise linear in size() - max_size
     */
    void set_max_size(size_type max_size)
    {
        this->max = max_size;
        cleanup();
    }

    /**
     * \brief Remove all elements
     *
     * Complexity: linear
     */
    void clear()
    {
        this->map.clear();
        this->usage.clear();
    }

    /**
     * \brief Retrieve the item at the given key
     * \throws std::out_of_range if not found
     *
     * Complexity: constant (on average)
     */
    mapped_type& at(const key_type& key)
    {
        // This retrieves the node or throws out_of_range
        Node& node = this->map.at(key);
        // Update access to the key
        this->touch(node);
        // Return the reference
        return node.value;
    }

    /**
     * \brief Inserts a new value
     *
     * Complexity: constant (on average)
     */
    void insert(const key_type& key, mapped_type value)
    {
        auto map_iterator = this->map.find(key);

        // If the key already exists, move-assign the value
        if ( map_iterator != this->map.end() )
        {
            this->touch(map_iterator->second);
            map_iterator->second.value = std::move(value);
        }
        // Other wise add a new item
        else
        {
            // Update usage
            this->usage.push_front(key);
            // Add the new node to the map
            this->map.emplace(key, Node{this->usage.begin(), std::move(value)});

            // The map might be bigger by one element
            cleanup();
        }
    }

    /**
     * \brief Checks whether a key exists
     *
     * Complexity: constant (on average)
     */
    bool contains(const key_type& key) const
    {
        return this->map.count(key);
    }

private:
    void cleanup()
    {
        if ( this->max > 0 )
        {
            while ( this->map.size() > this->max )
                this->pop_back();
        }
    }
};


/**
 * \brief Least-recently used cache for shared pointers
 */
template <class Key, class T, class Hash = Hasher<Key>,
          class Pointer=std::shared_ptr<T>, class Weak=std::weak_ptr<T>>
class SharedObjectLruCache : public detail::LruCacheCommon<Key, Pointer, Hash>
{
private:
    using Parent = detail::LruCacheCommon<Key, Pointer, Hash>;
    using Node = typename Parent::Node;

public:
    using key_type = typename Parent::key_type;
    using mapped_type = typename Parent::mapped_type;
    using size_type = typename Parent::size_type;

    explicit SharedObjectLruCache(size_type max_size)
        : Parent(max_size)
    {}

    /**
     * \brief Update the number of allowed items
     *
     * Complexity: constant if \p max_size >= size()
     * otherwise linear in size() - max_size
     */
    void set_max_size(size_type max_size)
    {
        this->max = max_size;
        cleanup();
    }

    /**
     * \brief Remove all elements
     *
     * Complexity: linear
     */
    void clear()
    {
        for ( const auto& pair : this->map )
        {
            lingering_check(pair);
        }
        this->map.clear();
        this->usage.clear();
    }

    /**
     * \brief Retrieve the item at the given key
     * \throws std::out_of_range if not found
     *
     * Complexity: constant (on average)
     */
    mapped_type& at(const key_type& key)
    {
        auto iter = this->map.find(key);
        if ( iter == this->map.end() && lingering.count(key) )
        {
            // this will insert lingering items
            contains(key);
            iter = this->map.find(key);
        }

        if ( iter == this->map.end() )
        {
            throw std::out_of_range("Item not found");
        }

        // Update access to the key
        this->touch(iter->second);
        // Return the reference
        return iter->second.value;
    }

    /**
     * \brief Inserts a new value
     *
     * Complexity: constant (on average)
     */
    void insert(const key_type& key, mapped_type value)
    {
        auto map_iterator = this->map.find(key);

        // If the key already exists, move-assign the value
        if ( map_iterator != this->map.end() )
        {
            this->touch(map_iterator->second);
            map_iterator->second.value = std::move(value);
            lingering.erase(key);
        }
        // Other wise add a new item
        else
        {
            insert_new(key, value);
        }
    }

    /**
     * \brief Checks whether a key exists
     * \param check_lingering If a lingering pointer is found,
     *                        it is added back to the cache
     *
     * Complexity: constant (on average)
     */
    bool contains(const key_type& key, bool check_lingering=true)
    {
        if ( this->map.count(key) )
            return true;
        if ( !check_lingering )
            return false;
        auto iter = lingering.find(key);
        if ( iter == lingering.end() )
            return false;
        auto ptr = iter->second.lock();
        if ( !ptr )
            return false;
        insert_new(key,  std::move(ptr));
        return true;
    }

    /**
     * \brief Size, including lingering shared references
     */
    size_type size_shared() const
    {
        return this->map.size() + std::count_if(
            lingering.begin(), lingering.end(),
            [](typename WeakMap::const_reference pair) {
                return !pair.second.expired();
            }
        );
    }

private:
    void insert_new(const key_type& key, mapped_type value)
    {
        // Update usage
        this->usage.push_front(key);
        // Add the new node to the map
        this->map.emplace(key, Node{this->usage.begin(), std::move(value)});

        lingering.erase(key);

        // The map might be bigger by one element
        cleanup();

    }

    void cleanup()
    {
        if ( this->max > 0 )
        {
            while ( this->map.size() > this->max )
            {
                lingering_check(*this->map.find(this->usage.back()));
                this->pop_back();
            }
        }

        for ( auto iter = lingering.begin(); iter != lingering.end(); )
        {
            if ( iter->second.expired() )
                iter = lingering.erase(iter);
            else
                ++iter;
        }
    }

    void lingering_check(const typename Parent::Map::value_type& pair)
    {
        auto iter = lingering.find(pair.first);
        bool has_lingering = iter != lingering.end();
        bool has_other_owners = pair.second.value.use_count() > 1;

        if ( has_lingering == has_other_owners )
            return;

        if ( has_other_owners )
            lingering.insert({pair.first, Weak(pair.second.value)});
        else
            lingering.erase(iter);
    }

    using WeakMap = std::unordered_map<key_type, Weak, Hash>;
    WeakMap lingering;
};

} // namespace melanolib
#endif // MELANOLIB_MELANOLIB_DATA_STRUCTURES_CACHE_HPP
